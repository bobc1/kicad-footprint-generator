# KicadModTree is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# KicadModTree is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with kicad-footprint-generator. If not, see < http://www.gnu.org/licenses/ >.
#
# (C) 2016-2018 by Thomas Pointhuber, <thomas.pointhuber@gmx.at>


class KiCadVars:

    symbol_dir_var = "${KICAD6_SYMBOL_DIR}"
    footprint_dir_var = "${KICAD6_FOOTPRINT_DIR}"
    _3D_model_dir_var = "${KICAD6_3DMODEL_DIR}"
    template_dir_var = "${KICAD6_TEMPLATE_DIR}"

    user_template_dir_var = "${KICAD_USER_TEMPLATE_DIR}"
